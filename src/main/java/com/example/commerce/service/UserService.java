package com.example.commerce.service;

import com.example.commerce.exception.usersexceptions.UserAlreadyExistsException;
import com.example.commerce.exception.usersexceptions.UserException;
import com.example.commerce.model.Users;
import com.example.commerce.registration.body.RegistrationBody;

import java.util.List;
import java.util.Optional;

public interface UserService {

    Users save(Users users) throws UserAlreadyExistsException;
    Users updateUser(Users users)throws UserException;
    Users removeUser(Long id) throws UserException;
    List<Users> getAllUsers();
    Users registerUser(RegistrationBody registrationBody) throws UserAlreadyExistsException;
}
