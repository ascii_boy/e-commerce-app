package com.example.commerce.service;

import com.example.commerce.exception.usersexceptions.UserAlreadyExistsException;
import com.example.commerce.exception.usersexceptions.UserException;
import com.example.commerce.model.Users;
import com.example.commerce.registration.body.RegistrationBody;
import com.example.commerce.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Users save(Users users) throws UserAlreadyExistsException {
        Optional<Users> check = userRepository.findById(users.getId());
        if (check.isPresent()) {
            throw new UserAlreadyExistsException();
        } else {

            return userRepository.save(users);
        }
    }

    @Override
    public Users updateUser(Users users) throws UserException {
        Optional<Users> load = userRepository.findById(users.getId());
        if (load.isPresent()) {
            return userRepository.save(users);
        }
        else {
            throw new UserException("No such user exists ");
        }
    }

    @Override
    public Users removeUser(Long id) throws UserException{
        Optional<Users> load = userRepository.findById(id);
        if (load.isPresent()) {
            Users removed = load.get();
            userRepository.delete(removed);
            return removed;
        }
        else {
            throw new UserException("No such user found");
        }
     }

    @Override
    public List<Users> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public Users registerUser(RegistrationBody registrationBody) throws UserAlreadyExistsException {
        if (userRepository.findByEmailIgnoreCase(registrationBody.getEmail()).isPresent() ||
            userRepository.findByUsernameIgnoreCase(registrationBody.getUsername()).isPresent()) {
            throw  new UserAlreadyExistsException();
        }
        else {
            Users users = new Users();
            users.setEmail(registrationBody.getEmail());
            users.setFirstname(registrationBody.getFirstname());
            users.setLastname(registrationBody.getLastname());
            users.setUsername(registrationBody.getUsername());
            users.setPassword(registrationBody.getPassword());
            return userRepository.save(users);
        }
    }
}
