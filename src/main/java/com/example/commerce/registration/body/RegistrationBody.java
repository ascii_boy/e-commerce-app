package com.example.commerce.registration.body;

import jakarta.validation.constraints.*;
import lombok.Data;


@Data
public class RegistrationBody {


    @NotNull
    @NotBlank
    @Size(min = 3, max = 255)
    private String username;

    @NotBlank
    @NotNull
    @Email
    private String email;

    @NotNull
    @NotBlank
    @Pattern(regexp = "^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$")
    @Size(min = 6, max = 32)
    private String password;

    @NotNull
    @NotBlank
    private String firstname;

    @NotNull
    @NotBlank
    private String lastname;


}
