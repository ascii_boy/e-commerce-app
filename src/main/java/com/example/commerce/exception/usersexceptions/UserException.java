package com.example.commerce.exception.usersexceptions;

public class UserException extends Exception {
    public UserException(String message) {
        super(message);
    }

    public UserException() {
    }
}
